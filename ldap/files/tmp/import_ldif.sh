#!/bin/bash

/etc/init.d/slapd start
ldapmodify -Y EXTERNAL -H ldapi:/// -f /tmp/config/ca-certificate.ldif || exit 1
ldapmodify -Y EXTERNAL -H ldapi:/// -f /tmp/config/tls.ldif # This will always fail, but make the subesquent command work for some insane reason
ldapmodify -Y EXTERNAL -H ldapi:/// -f /tmp/config/tls-reverse.ldif || exit 1
ldapmodify -Y EXTERNAL -H ldapi:/// -f /tmp/config/root.ldif || exit 1
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/config/member-of.ldif || exit 1
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/schema/ldap-public-key.ldif || exit 1
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/schema/licham-user.ldif || exit 1
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/schema/samba.ldif || exit 1
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/config/smbk5pwd.ldif || exit 1 # This must happen after samba.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /tmp/config/pw-sha2.ldif || exit 1
/etc/init.d/slapd stop
