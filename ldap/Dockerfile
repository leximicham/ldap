FROM ubuntu:22.04
ARG ROOT_PW_HASH=MISSING_SECRETS

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y \
      ca-certificates \
      ldap-utils \
      openssl \
      slapd \
      slapd-smbk5pwd \
    && rm -rf /var/apt/lists

# Issue a self signed cert so the service doesn't crash on bootstrap
RUN mkdir -p /etc/ssl/ldap
RUN openssl req \
    -x509 \
    -newkey rsa:4096 \
    -keyout /etc/ssl/ldap/tls.key \
    -out /etc/ssl/ldap/tls.crt \
    -sha256 \
    -days 365 \
    -nodes \
    -subj '/CN=localhost'
RUN cp /etc/ssl/certs/ca-certificates.crt /etc/ssl/ldap/ca.crt
RUN chown openldap:openldap /etc/ssl/ldap/*

COPY files/ /
RUN sed -i "s%MISSING_ROOT_PW_HASH%${ROOT_PW_HASH}%" /tmp/config/root.ldif
RUN /tmp/import_ldif.sh
RUN rm -rf /tmp/import_ldif.sh /tmp/config /tmp/schema

ENTRYPOINT /usr/sbin/slapd -h 'ldaps://0.0.0.0:10636' -g openldap -u openldap -F /etc/ldap/slapd.d -d stats
